const getSum = (str1, str2) => {
  if (str1.length > str2.length) {
    let t = str1;
    str1 = str2;
    str2 = t;
  }
  let str = "";
  let n1 = str1.length,
    n2 = str2.length;
  str1 = str1.split("").reverse().join("");
  str2 = str2.split("").reverse().join("");
  let carry = 0;
  for (let i = 0; i < n1; i++) {
    let sum =
      str1[i].charCodeAt(0) -
      "0".charCodeAt(0) +
      (str2[i].charCodeAt(0) - "0".charCodeAt(0)) +
      carry;
    str += String.fromCharCode((sum % 10) + "0".charCodeAt(0));
    carry = Math.floor(sum / 10);
  }
  for (let i = n1; i < n2; i++) {
    let sum = str2[i].charCodeAt(0) - "0".charCodeAt(0) + carry;
    str += String.fromCharCode((sum % 10) + "0".charCodeAt(0));
    carry = Math.floor(sum / 10);
  }
  if (carry > 0) str += String.fromCharCode(carry + "0".charCodeAt(0));
  str = str.split("").reverse().join("");
  return str;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let att = 0;
  let com = 0;
  listOfPosts.forEach(({ author, comments }) => {
    att += author === authorName;
    if (comments)
      com += comments.filter(({ author }) => author === authorName).length;
  });
  return `Post:${att},comments:${com}`;
};

const tickets = (peopleInLine) => {
  var a25 = 0,
    a50 = 0;
  for (var i = 0; i < peopleInLine.length; i++) {
    if (peopleInLine[i] == 25) {
      a25 += 1;
    }
    if (peopleInLine[i] == 50) {
      a25 -= 1;
      a50 += 1;
    }
    if (peopleInLine[i] == 100) {
      if (a50 == 0 && a25 >= 3) {
        a25 -= 3;
      } else {
        a25 -= 1;
        a50 -= 1;
      }
    }
    if (a25 < 0 || a50 < 0) {
      return "NO";
    }
  }
  return "YES";
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
